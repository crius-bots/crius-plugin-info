package crius_plugin_info

// We'll use realm instead of guild / channel here because we support both GuildCrius *and* LiveCrius
const tableCreateScript = `CREATE TABLE IF NOT EXISTS info__info
(
    info_id   serial primary key,
    realm_id  text not null,
    platform  text not null,
    info_name text not null,
    info      text not null
);

CREATE TABLE IF NOT EXISTS info__rules
(
    realm_id text not null,
    platform text not null,
    rules    text not null,
    constraint info__rules__pk primary key (realm_id, platform)
);
`

type InfoItem struct {
	InfoID   int    `db:"info_id"`
	RealmID  string `db:"realm_id"`
	InfoName string `db:"info_name"`
	Info     string
	Platform string `db:"platform"`
}
